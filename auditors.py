# file for comparing API and ADAT results

# imports
from importlib import reload
import csv
import json
import os
from fuzzywuzzy import fuzz

# get protocols from secondaries file
with open('protocols.txt', 'r') as pointer:

    # get all secondaries
    secondaries = [protocol.strip('\n') for protocol in pointer.readlines()]
    secondaries = [protocol for protocol in secondaries if 'X' in protocol]
    secondaries = [protocol.strip('X').strip() for protocol in secondaries]

# set up mosquitoes fields
mosquitoes = {' org_name': 'organizationName'}
mosquitoes.update({'mosquito habitat mapper:measured at': 'mosquitohabitatmapperMeasuredAt'})
mosquitoes.update({' site_id':  'siteId', 'mosquito habitat mapper:userid': 'mosquitohabitatmapperUserid'})
mosquitoes.update({' mosquito habitat mapper:mosquito habitat mapper id': 'mosquitohabitatmapperMosquitoHabitatMapperId'})

# set up macroinvertebrates fields
macroinvertebrates = {'organization_id': 'organizationId', ' site_id': 'siteId'}
macroinvertebrates.update({'freshwater macroinvertebrates:measured on': 'freshwatermacroinvertebratesMeasuredOn'})
macroinvertebrates.update({'freshwater macroinvertebrates:taxon latin name': 'freshwatermacroinvertebratesTaxonLatinName'})

# set up landcover fields
landcovers = {' org_name': 'organizationName', ' site_name': 'siteName'}
landcovers.update({'land covers:measured at': 'landcoversMeasuredAt'})
landcovers.update({' land covers:land cover id': 'landcoversLandCoverId'})

# set up surfacetemperatures
surfacetemps = {' org_name': 'organizationName', ' site_name': 'siteName'}
surfacetemps.update({'surface temperatures:measured at': 'surfacetemperaturesMeasuredAt'})
surfacetemps.update({' surface temperatures:userid': 'surfacetemperaturesUserid'})

# set up vegetationcovers
vegetation = {' org_name': 'organizationName', ' site_name': 'siteName'}
vegetation.update({'vegatation covers:measured on': 'vegatationcoversMeasuredOn'})
vegetation.update({' vegatation covers:userid': 'vegatationcoversUserid'})

# set up dissolved oxygen
oxygen = {' org_name': 'organizationName', ' site_name': 'siteName'}
oxygen.update({'dissolved oxygens:measured at': 'dissolvedoxygensMeasuredAt'})
oxygen.update({' dissolved oxygens:userid': 'dissolvedoxygensUserid'})

# set up air temp dailies
dailies = {}
dailies.update({' org_name': 'organizationName'})
dailies.update({' site_name': 'siteName'})
dailies.update({'air temp dailies:measured at': 'airtempdailiesMeasuredAt'})
dailies.update({' air temp dailies:userid': 'airtempdailiesUserid'})

# set up humidities monthlies
monthlies = {}
monthlies.update({' org_name': 'organizationName'})
monthlies.update({' site_name': 'siteName'})
monthlies.update({'humidity monthlies:userid': 'humiditymonthliesUserid'})
monthlies.update({' measured_on': 'measuredDate'})
monthlies.update({' site_id': 'siteId'})
monthlies.update({'humidity monthlies:number of obs': 'humiditymonthliesNumberOfObs'})
monthlies.update({'humidity monthlies:minimum dewpoint (deg C)': 'humiditymonthliesMinimumDewpointC'})
monthlies.update({'humidity monthlies:max relative humidity (%)': 'humiditymonthliesMaxRelativeHumidityPercent'})
monthlies.update({'humidity monthlies:maximum dewpoint (deg C)': 'humiditymonthliesMaximumDewpointC'})
monthlies.update({' longitude': 'longitude'})
monthlies.update({' latitude': 'latitude'})
monthlies.update({' elevation': 'elevation'})
monthlies.update({' humidity monthlies:averaged month': 'humiditymonthliesAveragedMonth'})
monthlies.update({'humidity monthlies:average dewpoint (deg C)': 'humiditymonthliesAverageDewpointC'})
monthlies.update({'organization_id': 'organizationId'})
monthlies.update({'humidity monthlies:number of days reported': 'humiditymonthliesNumberOfDaysReported'})

# set up surface temperatures
surfaces = {}
surfaces.update({' org_name': 'organizationName'})
surfaces.update({' site_id': 'siteId'})
surfaces.update({'surface temperatures:solar measured at': 'surfacetemperaturesSolarMeasuredAt'})
surfaces.update({'surface temperatures:measured at': 'surfacetemperaturesMeasuredAt'})
surfaces.update({' surface temperatures:userid': 'surfacetemperaturesUserid'})


# set up protocols
protocols = {protocol: {} for protocol in secondaries}
protocols.update({'freshwater_macroinvertebrates': macroinvertebrates, 'mosquito_habitat_mapper': mosquitoes})
protocols.update({'land_covers': landcovers, 'surface_temperatures': surfacetemps, 'vegatation_covers': vegetation})
protocols.update({'dissolved_oxygen': oxygen, 'air_temp_dailies': dailies})
protocols.update({'humidity_monthlies': monthlies, 'surface_temperatures': surfaces})


# infer an item from a list through fuzzy matching
def infer(word, collection):
    """Infer the closest match to a word from a collection.

    Arguments:
        word: str,
        collection: list of str

    Returns:
        str
    """

    # compare the ratio against all members
    ratios = [(member, fuzz.ratio(word, member)) for member in collection]
    ratios.sort(key=lambda pair: pair[1], reverse=True)

    # get the best match
    match = ratios[0][0]

    return match


# extract members of a list with a certain field value
def extract(field, value, records):
    """Extract the members where the field is a value from a list of records.

    Arguments:
        field: str, rough guess at field
        value: str or float, value of the field
        records: list of dicts.

    Returns:
        list of dicts
    """

    # find best match for the field
    match = infer(field, records[0].keys())

    # extract
    extracts = [record for record in records if record[match] == value]

    return extracts


# compare two records based on their values
def compare(record, recordii):
    """Compare two records to see which datums differ.

    Arguments:
        record: dict
        recordii: dict

    Returnss
        None
    """

    # print absent from first
    absence = list(set([item for item in record.items() if item[1] not in recordii.values()]))
    absence.sort()
    print('\nabsent in first:')
    print(absence)

    # print absent from first
    absence = list(set([item for item in recordii.items() if item[1] not in record.values()]))
    absence.sort()
    print('\nabsent in second:')
    print(absence)

    return


# survey all members of a record for a particular field
def survey(field, records):
    """Survey all unique entries for a field for a group of records.

    Arguments:
        field: str, the field
        records: list of dicts

    Returns:
        None
    """

    # find best field name
    field = infer(field, records[0].keys())

    # get all entries
    entries = [record[field] for record in records]
    entries = list(set(entries))
    entries.sort()

    # print
    print('{} unique entries amongst {} records for field {}:'.format(len(entries), len(records), field))
    print(entries)

    return None


# align up field names from differ
def align(adats, apis, name=None):
    """Align the field names with their closest matches.

    Arguments:
        adats: list of dicts
        apis: list of dicts
        name: str, name of variable

    Returns:
        None
    """

    # get the field names
    fields = [field for field in adats[0].keys()]
    fieldsii = [field for field in apis[0].keys()]

    # make alignments for each
    alignments = [(field, infer(field, fieldsii)) for field in fields]
    alignments += [(infer(field, fields), field) for field in fieldsii]

    # remove duplicates
    alignments = list(set(alignments))
    alignments.sort()

    # turn into dictionaries
    alignments = [str({field: fieldii}) for field, fieldii in alignments]

    # add name
    if name:

        # add variable update name
        alignments = ['{}.update({})'.format(name, alignment) for alignment in alignments]

    # print
    [print(alignment) for alignment in alignments]

    return None


# compare the records
def retrieve(adat, api):
    """Retrieve the ADAT records and the API records from downloaded csvs.

    Arguments:
        adat: str, adat csv file name
        api: str, api csv file name

    Returns:
        tuple of lists of dicts
    """

    # begin table
    table = []

    # retrieve the adat csv
    path = '../../Downloads/' + adat
    with open(path, 'r') as pointer:

        # read in the csv rows
        rows = [row for row in csv.reader(pointer)]

    # construct records
    adats = []
    headers = rows[0]
    for row in rows[2:]:

        # make record
        record = {header: entry for header, entry in zip(headers, row)}
        adats.append(record)

    # retrieve the api csv
    stub = '_'.join(api.split('_')[-2:]).split('.')[0]
    directory = 'mosquitoes_bundle_' + stub + '/'
    path = '../../Downloads/' + directory + api
    with open(path, 'r') as pointer:

        # read in the csv rows
        rows = [row for row in csv.reader(pointer)]

    # construct records
    apis = []
    headers = rows[0]
    for row in rows[1:]:

        # make record
        record = {header: entry for header, entry in zip(headers, row)}
        apis.append(record)

    # retrieve summary
    path = '../../Downloads/' + directory + 'summary_' + stub + '.txt'
    with open(path, 'r') as pointer:

        # read in the csv rows
        lines = [line.strip() for line in pointer.readlines()]

    # extract dates
    dates = lines[1].replace('r', 'R').replace('d', 'D')

    # extract lat longs
    points = []
    countries = []
    for line in lines[3:]:

        # check for brackets
        if '[' and ']' in line:

            # get lat long coordinates and country name
            point, country = line.split(':')
            point = json.loads(point)
            points.append(point)
            countries.append(country.strip())

    # extract points
    longitudes = [point[0] for point in points]
    latitudes = [point[1] for point in points]

    # combine countries
    countries = '({})'.format(', '.join([country for country in set(countries) if country]))

    # print summary
    print(dates)
    table.append([dates])
    print('Latitude: {} to {}'.format(min(latitudes), max(latitudes)))
    print('Longitude: {} to {}'.format(min(longitudes), max(longitudes)))
    table.append(['Latitudes: {} to {}'.format(min(latitudes), max(latitudes))])
    table.append(['Longitudes: {} to {}'.format(min(latitudes), max(latitudes))])
    print(countries)
    table.append([str(countries)])

    # print status
    print(' ')
    print('ADAT records: {}'.format(len(adats)))
    print('API records: {}'.format(len(apis)))
    table.append(['ADAT records: {}'.format(len(adats))])
    table.append(['API records: {}'.format(len(apis))])

    return adats, apis, table


# audit the adats and apis against one another
def audit(adats, apis, fields, details=False):
    """Audit the ADAT records against the API records for records missing in each.

    Arguments:
        adats: list of dicts
        apis: list of dicts
        fields: dict of field names that must match for records to match
        details: boolean, report details?

    Returns:
        tuple of lists of dicts, the missing records
    """

    # begin table
    table = []

    # convert to tuples
    extracts = [tuple([record[field] for field in fields.keys()]) for record in adats]
    extractsii = [tuple([record[field] for field in fields.values()]) for record in apis]

    # get records lost from adat
    indices = [index for index, extract in enumerate(extractsii) if extract not in extracts]
    lost = [apis[index] for index in indices]

    # get records lost from api
    indices = [index for index, extract in enumerate(extracts) if extract not in extractsii]
    lostii = [adats[index] for index in indices]

    # report
    print(' ')
    print('{} records missing from ADAT'.format(len(lost)))
    print('{} records missing from API'.format(len(lostii)))
    print(' ')
    table.append(['{} records missing from ADAT'.format(len(lost))])
    table.append(['{} records missing from API'.format(len(lostii))])

    # report fields?
    if details:

        # report lists of each field
        for field, fieldii in fields.items():

            # print field
            print('\n{} / {}:'.format(field, fieldii))

            # find present in ADATs
            presence = list(set([record[field] for record in adats]))
            presence.sort()

            # find absent in ADATs
            absence = list(set([record[fieldii] for record in lost if record[fieldii] not in presence]))
            absence.sort()
            print('\nabsent in ADAT:')
            print(absence)

            # find present in APIs
            presence = list(set([record[fieldii] for record in apis]))
            presence.sort()

            # find absent in APIs
            absence.sort()
            absence = list(set([record[field] for record in lostii if record[field] not in presence]))
            print('\nabsent in API:')
            print(absence)

    # check times
    rows = synchronize(lost, apis, adats)
    table += rows

    return lost, lostii, table


# check the measured at times in lost records against present records
def synchronize(lost, apis, adats):
    """Synchronize the times of lost records with original set of records to look for same times.

    Arguments:
        lost: list of dicts
        apis: list of dicts
        adats: list of dicts

    Returns:
        None
    """

    # begin table
    table = []

    # get protocol
    protocol = infer(lost[0]['protocol'], protocols.keys())

    # get api fields
    measured = infer(protocol + 'measuredat', apis[0].keys())
    organization = infer('organizationname', apis[0].keys())
    site = infer('siteName', apis[0].keys())
    user = infer(protocol + 'userid', apis[0].keys())
    base = [measured, organization, site, user]
    headers = [key for key in apis[0].keys()]
    headers = [header for header in headers if header not in base]
    headers.sort(key=lambda word: len(word))
    headers = base + headers

    # get adat fields
    measuredii = infer(protocol + 'measuredat', adats[0].keys())
    organizationii = infer('orgname', adats[0].keys())
    siteii = infer('siteName', adats[0].keys())
    userii = infer(protocol + 'userid', adats[0].keys())
    baseii = [measuredii, organizationii, siteii, userii]
    headersii = [key for key in adats[0].keys()]
    headersii = [header for header in headersii if header not in baseii]
    headersii.sort(key=lambda word: len(word))
    headersii = baseii + headersii

    # go through each lost record:
    missing = []
    for index, record in enumerate(lost[:2]):

        # print and add to table
        print('\n# {} of {}:'.format(index, len(lost)))
        table.append([''])
        table.append(['# {} of {}:'.format(index, len(lost))])
        print('\nMissing from ADAT:')
        table.append(['Missing from ADAT:'])
        info = tuple([record[measured], record[organization][:15], record[site][:15], record[user]])
        print('Time: {}, Org: {}, Site: {}, user: {}'.format(*info))
        table.append(headers)
        table.append([record[header] for header in headers])

        # extract other records
        table.append([''])
        print('\nAPI records with same time:')
        table.append(['API records with same time:'])
        table.append(headers)
        extracts = extract(measured, record[measured], apis)
        extracts.sort(key=lambda member: member[user] == record[user], reverse=True)
        matches = []
        for recordii in extracts:

            # print info
            infoii = tuple([recordii[measured], recordii[organization][:15], recordii[site][:15], recordii[user]])
            print('Time: {}, Org: {}, Site: {}, user: {}'.format(*infoii))
            matches.append(recordii)
            table.append([recordii[header] for header in headers])

        # extract other records
        table.append([''])
        print('\nADAT records with same time:')
        table.append(['ADAT records with same time:'])
        table.append(headersii)
        extracts = extract(measuredii, record[measured], adats)
        extracts.sort(key=lambda member: member[userii] == record[user], reverse=True)
        for recordii in extracts:

            # print info
            infoii = tuple([recordii[measuredii], recordii[organizationii][:15], recordii[siteii][:15], recordii[userii]])
            print('Time: {}, Org: {}, Site: {}, user: {}'.format(*infoii))
            table.append([recordii[header] for header in headersii])

        # add to missing
        if len(matches) < 2:

            # add to missing
            missing.append(record)

    # print status
    print('\n{} records not matched.'.format(len(missing)))

    return table


# report on latest run
def report(protocol, auditor=True, add=False):
    """Report on the latest run for a particular protocol.

    Arguments:
        protocol: str, protocol name
        auditor=True: boolean, audit records?
        add=False: boolean, add to csv table?

    Returns:
        tuple of lists of dicts
    """

    # get file for appending
    path = 'adat_api_comparison.csv'
    with open(path, 'r') as pointer:

        # readrows
        table = [row for row in csv.reader(pointer)]

    # begin csv rows
    table += [[''], ['']]

    # find latest adat file in downloads directory
    downloads = os.listdir('../../Downloads')
    names = [name for name in downloads if 'GLOBEMeasurementData' in name and '.csv' in name]
    names.sort(reverse=True)
    adat = names[0]

    # find latest api zip file
    names = [name for name in downloads if 'mosquitoes_bundle' in name and '.zip' in name]
    names.sort(reverse=True)
    directory = names[0].replace('.zip', '')

    # infer the protocol from the list
    protocol = infer(protocol, protocols.keys())

    # get the file from the directory
    bundle = os.listdir('../../Downloads/' + directory)
    names = [name for name in bundle if protocol in name]
    api = names[0]

    # print
    print('retrieving {} and {}'.format(adat, api))

    # print protocol
    print('\nProtocol: {}'.format(protocol))
    table.append(['Protocol: {}'.format(protocol)])

    # retreve the data
    adats, apis, rows = retrieve(adat, api)
    table += rows

    # check against missing records
    lost = None
    lostii = None
    if auditor:

        # check for missing records
        lost, lostii, rows = audit(adats, apis, protocols[protocol])
        table += rows

    # write table to csv
    if add:

        # rewrite csv
        with open(path, 'w') as pointer:

            # writerows
            [csv.writer(pointer).writerow(row) for row in table]

    return adats, apis, lost, lostii



